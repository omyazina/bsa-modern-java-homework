package com.binary_studio.academy_coin;

import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		ExtremasList extremasPrices = new ExtremasListImpl(prices);

		int profit = extremasPrices.getList().stream().mapToInt(Integer::intValue).sum();

		return profit;
	}

}
