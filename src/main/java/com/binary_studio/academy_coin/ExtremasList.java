package com.binary_studio.academy_coin;

import java.util.List;

public interface ExtremasList {

	List<Integer> getList();

}
