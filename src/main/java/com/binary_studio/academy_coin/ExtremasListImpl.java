package com.binary_studio.academy_coin;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import com.binary_studio.academy_coin.model.PointWithDerivative;

public class ExtremasListImpl implements ExtremasList {

	private List<Integer> extremas;

	private List<PointWithDerivative> derivatives;

	public ExtremasListImpl(Stream<Integer> stream) {
		this.derivatives = getDerivatives(stream);
		this.extremas = getExtremasFromDerivatives(this.derivatives);
	}

	public ExtremasListImpl(List<Integer> list) {
		this.derivatives = getDerivatives(list.stream());
		this.extremas = getExtremasFromDerivatives(this.derivatives);
	}

	public List<Integer> getList() {
		return this.extremas;
	}

	private List<PointWithDerivative> getDerivatives(Stream<Integer> points) {
		Objects.requireNonNull(points);
		List<PointWithDerivative> derivatives = new ArrayList<>();

		points.forEach((value) -> {
			int delta;
			int derivativesLength = derivatives.size();

			if (derivativesLength == 0) {
				delta = -value;
			}
			else {
				delta = value - derivatives.get(derivativesLength - 1).getValue();
			}

			if (delta != 0 || derivativesLength == 0) {
				derivatives.add(new PointWithDerivative(value, delta));
			}
		});

		return derivatives;
	}

	private List<Integer> getExtremasFromDerivatives(List<PointWithDerivative> derivatives) {
		Objects.requireNonNull(derivatives);
		List<Integer> extremas = new ArrayList<>();

		for (int i = 0; i < derivatives.size(); i++) {
			int pointValue = derivatives.get(i).getValue();
			int pointDelta = derivatives.get(i).getDelta();

			boolean isLastItem = (i == derivatives.size() - 1);

			int nextPointDelta;
			boolean isExtremaMin = false;
			boolean isExtremaMax = false;

			if (isLastItem) {
				isExtremaMax = pointDelta > 0;
			}
			else {
				nextPointDelta = derivatives.get(i + 1).getDelta();

				isExtremaMin = pointDelta < 0 && nextPointDelta > 0;
				isExtremaMax = pointDelta > 0 && nextPointDelta < 0;
			}

			pointValue = isExtremaMin ? -pointValue : pointValue;

			if (isExtremaMin || isExtremaMax) {
				extremas.add(pointValue);
			}
		}

		return extremas;
	}

}
