package com.binary_studio.academy_coin.model;

public class PointWithDerivative {

	private int value;

	private int delta;

	public PointWithDerivative(int value, int delta) {
		this.value = value;
		this.delta = delta;
	}

	public int getValue() {
		return this.value;
	}

	public int getDelta() {
		return this.delta;
	}

	@Override
	public String toString() {
		return String.format("(%d , %d)", this.value, this.delta);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof PointWithDerivative)) {
			return false;
		}

		PointWithDerivative p = (PointWithDerivative) o;

		return Integer.compare(this.value, p.value) == 0 && Integer.compare(this.delta, p.delta) == 0;
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + Integer.valueOf(this.value).hashCode();
		result = 31 * result + Integer.valueOf(this.delta).hashCode();
		return result;
	}

}
