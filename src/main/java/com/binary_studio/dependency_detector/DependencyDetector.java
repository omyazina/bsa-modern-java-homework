package com.binary_studio.dependency_detector;

import com.binary_studio.dependency_detector.graph.Graph;
import com.binary_studio.dependency_detector.graph.GraphImpl;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		Graph graph = new GraphImpl(libraries.dependencies);

		return !graph.hasCycle();
	}

}
