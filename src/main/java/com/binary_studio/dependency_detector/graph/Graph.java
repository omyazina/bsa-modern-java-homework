package com.binary_studio.dependency_detector.graph;

public interface Graph {

	boolean hasCycle();

}
