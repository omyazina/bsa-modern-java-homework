package com.binary_studio.dependency_detector.graph;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class GraphImpl implements Graph {

	private Map<String, Vertex> graph = new HashMap<>();

	public GraphImpl(List<String[]> edges) {
		this.graph = buildGraph(edges);
	}

	@Override
	public boolean hasCycle() {
		boolean hasBackEdge = false;
		for (Vertex v : this.graph.values()) {
			if (!v.isVisited() && hasVertexCycle(v)) {
				hasBackEdge = true;
				break;
			}
		}
		return hasBackEdge;
	}

	private Map<String, Vertex> buildGraph(List<String[]> edges) {
		Objects.requireNonNull(edges);
		Map<String, Vertex> newGraph = new HashMap<>();

		for (String[] edge : edges) {
			if (edge == null || edge.length < 2) {
				throw new IllegalArgumentException();
			}

			String vertexName = edge[0];
			String vertexSiblingName = edge[1];

			if (newGraph.containsKey(vertexName)) {
				newGraph.get(vertexName).addEdge(vertexSiblingName);
			}
			else {
				var v = new VertexImpl();
				v.addEdge(vertexSiblingName);
				newGraph.put(vertexName, v);
			}
		}

		return newGraph;
	}

	private boolean hasVertexCycle(Vertex vertex) {
		Objects.requireNonNull(vertex);

		boolean hasBackEdge = false;
		vertex.setVisited(true);

		for (String edgeSiblingName : vertex.getEdges()) {

			if (this.graph.containsKey(edgeSiblingName)) {
				var vertexSibling = this.graph.get(edgeSiblingName);

				if (vertexSibling.isVisited() || hasVertexCycle(vertexSibling)) {
					hasBackEdge = true;
					break;
				}
			}
		}

		return hasBackEdge;
	}

}
