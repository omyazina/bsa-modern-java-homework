package com.binary_studio.dependency_detector.graph;

import java.util.List;

public interface Vertex {

	boolean isVisited();

	void setVisited(boolean visited);

	List<String> getEdges();

	boolean addEdge(String vertexName);

}
