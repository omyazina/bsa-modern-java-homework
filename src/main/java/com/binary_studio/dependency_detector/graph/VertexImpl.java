package com.binary_studio.dependency_detector.graph;

import java.util.ArrayList;
import java.util.List;

public class VertexImpl implements Vertex {

	private boolean visited;

	private List<String> edges;

	public VertexImpl() {
		this.edges = new ArrayList<>();
	}

	@Override
	public boolean isVisited() {
		return this.visited;
	}

	@Override
	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	@Override
	public List<String> getEdges() {
		return this.edges;
	}

	@Override
	public boolean addEdge(String vertexName) {
		return this.edges.add(vertexName);
	}

	@Override
	public String toString() {
		return this.edges.toString();
	}

}
