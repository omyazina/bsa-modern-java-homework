package com.binary_studio.academy_coin_test;

import java.util.Arrays;
import java.util.stream.Stream;

import com.binary_studio.academy_coin.AcademyCoin;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AcademyCoinTest {

	public static final String TASK = "academy_coin";

	@Test
	@DisplayName("Two Spikes")
	void AcademyCoin_ProfitWithTwoSpikes_SpikeDiffSumReturned() {
		assertEquals(7, AcademyCoin.maxProfit(Arrays.stream(new int[] { 7, 1, 5, 3, 6, 4 }).boxed()),
				"Should be 7 for two spikes: buy on day 2 - sell on day 3(5-1 = 4), buy on day 4 - sell on day 5(6 - 3 = 3), equals to 7(4 + 3) in total profit");
	}

	@Test
	@DisplayName("No fall")
	void AcademyCoin_ProfitWithNoDecrease_FirstAndLastDiffReturned() {
		assertEquals(4, AcademyCoin.maxProfit(Arrays.stream(new int[] { 1, 2, 3, 4, 5 }).boxed()),
				"Should be 4: buy on day 1, sell on day 5 for 5-1 = 4. Notice, that you can't engage in multiple transactions");
	}

	@Test
	@DisplayName("No profit")
	void AcademyCoin_NoProfitAtAll_ZeroReturned() {
		assertEquals(0, AcademyCoin.maxProfit(Arrays.stream(new int[] { 7, 6, 4, 3, 1 }).boxed()),
				"There is no profit to be made, so return zero");
	}

	@Test
	@DisplayName("Sell on extrema max")
	void AcademyCoin_SellOnExtremaMax() {
		assertEquals(8, AcademyCoin.maxProfit(Arrays.stream(new int[] { 2, 3, 5, 10 }).boxed()),
				"Should be 8: buy on day 1 (-2), sell on day 4 (+10) = 8.");
	}

	@Test
	@DisplayName("Sell on extrema max in chain")
	void AcademyCoin_SellOnExtremaMaxInChain() {
		assertEquals(15, AcademyCoin.maxProfit(Arrays.stream(new int[] { 2, 3, 5, 10, 2, 5, 7, 9 }).boxed()),
				"Should be 15: buy on day 1 (-2), sell on day 4 (+10), buy on day 5 (-2), sell on day 8 (+9) = 15.");
	}

	@Test
	@DisplayName("Buy on extrema min")
	void AcademyCoin_BuyOnExtremaMin() {
		assertEquals(4, AcademyCoin.maxProfit(Arrays.stream(new int[] { 7, 5, 1, 5 }).boxed()),
				"Should be 4: buy on day 3 (-1), sell on day 4 (+5) = 4.");
	}

	@Test
	@DisplayName("Buy on extrema min in chain")
	void AcademyCoin_BuyOnExtremaMinInChain() {
		assertEquals(12, AcademyCoin.maxProfit(Arrays.stream(new int[] { 7, 5, 1, 5, 4, 3, 2, 10 }).boxed()),
				"Should be 12: buy on day 3 (-1), sell on day 4 (+5), buy on day 7 (-2), sell on day 8 (+10) = 12.");
	}

	@Test
	@DisplayName("No open position at the end")
	void AcademyCoin_NoOpenPositionAtTheEnd() {
		assertEquals(4, AcademyCoin.maxProfit(Arrays.stream(new int[] { 1, 3, 5, 4, 2, 1 }).boxed()),
				"Should be 4: buy on day 1 (-1), sell on day 3 (+5) = 4.");
	}

	@Test
	@DisplayName("Contains repetitive prices between extremas")
	void AcademyCoin_HasRepetitivedPricesBetweenExtremas() {
		assertEquals(11, AcademyCoin.maxProfit(Arrays.stream(new int[] { 7, 1, 3, 3, 3, 4, 2, 2, 1, 9 }).boxed()),
				"Should be 11: buy on day 2 (-1), sell on day 6 (+4), buy on day 9 (-1), sell on day 10 (+9) = 11.");
	}

	@Test
	@DisplayName("Contains repetitive prices to close a position")
	void AcademyCoin_ContainsRepetitivePricesToClosePosition() {
		assertEquals(3, AcademyCoin.maxProfit(Arrays.stream(new int[] { 4, 1, 3, 3, 1, 2 }).boxed()),
				"Should be 3: buy on day 2 (-1), sell on day 4 (+3), buy on day 5 (-1), sell on day 6 (+2) = 3.");
	}

	@Test
	@DisplayName("Contains repetitive prices to open a position")
	void AcademyCoin_ContainsRepetitivePricesToOpenPosition() {
		assertEquals(3, AcademyCoin.maxProfit(Arrays.stream(new int[] { 4, 1, 1, 3, 1, 2 }).boxed()),
				"Should be 3: buy on day 3 (-1), sell on day 4 (+3), buy on day 5 (-1), sell on day 6 (+2) = 3.");
	}

	@Test
	@DisplayName("Contains repetitive prices at start")
	void AcademyCoin_ContainsRepetitivePricesAtStart() {
		assertEquals(3, AcademyCoin.maxProfit(Arrays.stream(new int[] { 1, 1, 1, 3, 1, 2 }).boxed()),
				"Should be 3: buy on day 3 (-1), sell on day 4 (+3), buy on day 5 (-1), sell on day 6 (+2) = 3.");
	}

	@Test
	@DisplayName("Contains repetitive prices at the end")
	void AcademyCoin_ContainsRepetitivePricesAtEnd() {
		assertEquals(2, AcademyCoin.maxProfit(Arrays.stream(new int[] { 1, 2, 3, 1, 1, 1 }).boxed()),
				"Should be 2: buy on day 1 (-1), sell on day 3 (+3) = 2.");
	}

	@Test
	@DisplayName("Contains repetitive prices at the end with open position")
	void AcademyCoin_ContainsRepetitivePricesAtEndWithOpenPosition() {
		assertEquals(2, AcademyCoin.maxProfit(Arrays.stream(new int[] { 1, 2, 3, 3, 3 }).boxed()),
				"Should be 2: buy on day 1 (-1), sell on day 5 (+3) = 2.");
	}

	@Test
	@DisplayName("Free coins")
	void AcademyCoin_FreeCoins() {
		assertEquals(5, AcademyCoin.maxProfit(Arrays.stream(new int[] { 4, 0, 1, 5 }).boxed()),
				"Should be 5: buy on day 2 (0), sell on day 4 (+5) = 5.");
	}

	@Test
	@DisplayName("Free coins on start")
	void AcademyCoin_FreeCoinsOnStart() {
		assertEquals(5, AcademyCoin.maxProfit(Arrays.stream(new int[] { 0, 0, 1, 5 }).boxed()),
				"Should be 5: buy on day 2 (0), sell on day 4 (+5) = 5.");
	}

	@Test
	@DisplayName("Free coins at end")
	void AcademyCoin_FreeCoinsAtEnd() {
		assertEquals(4, AcademyCoin.maxProfit(Arrays.stream(new int[] { 1, 5, 0, 0 }).boxed()),
				"Should be 4: buy on day 1 (-1), sell on day 2 (+5) = 4.");
	}

	@Test
	@DisplayName("Free coins in middle")
	void AcademyCoin_FreeCoinsInMiddle() {
		assertEquals(6, AcademyCoin.maxProfit(Arrays.stream(new int[] { 1, 5, 0, 0, 2 }).boxed()),
				"Should be 6: buy on day 1 (-1), sell on day 2 (+5), buy on day 4 (0), sell on day 5 (+2) = 6.");
	}

	@Test
	@DisplayName("Free coins in middle with no open position")
	void AcademyCoin_FreeCoinsInMiddleWithNoOpenPosition() {
		assertEquals(2, AcademyCoin.maxProfit(Arrays.stream(new int[] { 1, 0, 0, 2 }).boxed()),
				"Should be 2: buy on day 3 (0), sell on day 4 (+2) = 2.");
	}

	@Test
	@DisplayName("Empty stream passed")
	void AcademyCoin_EmptyStreamPassed() {
		Stream<Integer> emptyStream = Stream.of();
		assertEquals(0, AcademyCoin.maxProfit(emptyStream), "Should be 0.");
	}

}
